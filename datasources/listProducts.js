module.exports = (hits, total) => {
	return {
		listTemplate2Metadata: {
			type: 'object',
			objectId: 'lt1Metadata',
			backgroundImage: {
				contentDescription: null,
				smallSourceUrl: null,
				largeSourceUrl: null,
				sources: [
					{
						url: 'https://as-public-images.s3.amazonaws.com/background_italika.jpg',
						size: 'large',
						widthPixels: 0,
						heightPixels: 0
					}
				]
			},
			title: 'Resultados para productos de "Italika"',
			logoUrl: 'https://as-public-images.s3.amazonaws.com/logo_italika.png'
		},
		listTemplate2ListData: {
			type: 'list',
			listId: 'lt2Sample',
			totalNumberOfItems: total,
			hintText: 'Inenta, "Alexa, selecciona el producto número 1"',
			listPage: {
				listItems: hits.map(({ _source, _id }, index) => ({
					listItemIdentifier: _id,
					textContent: {
						primaryText: {
							type: 'PlainText',
							text: _source.name
						},
						secondaryText: {
							type: 'PlainText',
							text: _source.slogan
						},
						footerText: {
							type: 'PlainText',
							text: `$ ${_source.price}`
						},
						index: {
							type: 'PlainText',
							text: index + 1
						}
					},
					image: {
						contentDescription: null,
						smallSourceUrl: null,
						largeSourceUrl: null,
						sources: [
							{
								url: _source.images[0],
								size: 'small',
								widthPixels: 0,
								heightPixels: 0
							}
						]
					}
				}))
			}
		}
	};
};
