module.exports = {
	bodyTemplateIntro: {
		type: 'object',
		objectId: 'bt7Sample',
		title: 'Imagen corporativa',
		backgroundImage: {
			contentDescription: null,
			smallSourceUrl: null,
			largeSourceUrl: null,
			sources: [
				{
					url: 'https://as-public-images.s3.amazonaws.com/background_italika.jpg',
					size: 'small',
					widthPixels: 0,
					heightPixels: 0
				},
				{
					url: 'https://as-public-images.s3.amazonaws.com/background_italika.jpg',
					size: 'large',
					widthPixels: 0,
					heightPixels: 0
				}
			]
		},
		image: {
			contentDescription: null,
			smallSourceUrl: null,
			largeSourceUrl: null,
			sources: [
				{
					url: 'https://as-public-images.s3.amazonaws.com/logo_italika.png',
					size: 'small',
					widthPixels: 0,
					heightPixels: 0
				},
				{
					url: 'https://as-public-images.s3.amazonaws.com/logo_italika.png',
					size: 'large',
					widthPixels: 0,
					heightPixels: 0
				}
			]
		},
		logoUrl: 'https://as-public-images.s3.amazonaws.com/logo_italika.png',
		hintText: ''
	}
};
