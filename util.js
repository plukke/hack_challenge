const AWS = require('aws-sdk');

const s3SigV4Client = new AWS.S3({
	signatureVersion: 'v4'
});

const esDomain = {
	endpoint: 'https://search-products-jy7ocahw6opw55pfz4tj3pnste.us-east-1.es.amazonaws.com',
	region: 'us-east-1'
};

const endpoint = new AWS.Endpoint(esDomain.endpoint);
const creds = new AWS.EnvironmentCredentials('AWS');

exports.emailRegex = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;

exports.getS3PreSignedUrl = function getS3PreSignedUrl(s3ObjectKey) {
	const bucketName = process.env.S3_PERSISTENCE_BUCKET;
	const s3PreSignedUrl = s3SigV4Client.getSignedUrl('getObject', {
		Bucket: bucketName,
		Key: s3ObjectKey,
		Expires: 60 * 1 // the Expires is capped for 1 minute
	});
	console.log(`Util.s3PreSignedUrl: ${s3ObjectKey} URL ${s3PreSignedUrl}`);
	return s3PreSignedUrl;
};

/* Detect support APL */
exports.supportsDisplay = (handlerInput) => {
	return (
		handlerInput.requestEnvelope.context !== undefined &&
		handlerInput.requestEnvelope.context.System !== undefined &&
		handlerInput.requestEnvelope.context.System.device !== undefined &&
		handlerInput.requestEnvelope.context.System.device.supportedInterfaces !== undefined &&
		(handlerInput.requestEnvelope.context.System.device.supportedInterfaces['Alexa.Presentation.APL'] !==
			undefined ||
			handlerInput.requestEnvelope.context.System.device.supportedInterfaces.Display !== undefined) &&
		handlerInput.requestEnvelope.context.Viewport !== undefined
	);
};

exports.requestES = (query, sort = []) => {
	const path = '/hack_challenge/_search';
	const body = JSON.stringify({ query, size: 5, sort: [ ...sort ] });

	return new Promise((resolve, reject) => {
		const req = new AWS.HttpRequest(endpoint);

		req.method = 'POST';

		/** Time to be open for scroll  */
		req.path = path;
		req.region = esDomain.region;
		req.body = body;
		req.headers['presigned-expires'] = false;
		req.headers['Host'] = endpoint.host;
		req.headers['Content-Length'] = req.body.length;
		req.headers['Content-Type'] = 'application/json';

		// Sign the request (Sigv4)
		var signer = new AWS.Signers.V4(req, 'es');
		signer.addAuthorization(creds, new Date());

		// Post document to ES
		var send = new AWS.NodeHttpClient();
		send.handleRequest(
			req,
			null,
			function(httpResp) {
				var body = '';
				httpResp.on('data', function(chunk) {
					body += chunk;
				});

				httpResp.on('end', function(chunk) {
					resolve(body);
				});
			},
			function(err) {
				console.log('Error: ' + err);
				reject(err);
			}
		);
	});
};

exports.callDirectiveService = (handlerInput, speech) => {
	const requestEnvelope = handlerInput.requestEnvelope;
	const directiveServiceClient = handlerInput.serviceClientFactory.getDirectiveServiceClient();

	const requestId = requestEnvelope.request.requestId;
	const endpoint = requestEnvelope.context.System.apiEndpoint;
	const token = requestEnvelope.context.System.apiAccessToken;

	// build the progressive response directive
	const directive = {
		header: {
			requestId
		},
		directive: {
			type: 'VoicePlayer.Speak',
			speech
		}
	};

	// send directive
	return directiveServiceClient.enqueue(directive, endpoint, token);
};
