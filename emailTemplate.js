const moment = require("moment");
const momenttz = require("moment-timezone");
moment.locale("es");
momenttz.tz.setDefault("America/Mexico_City");

module.exports = (subject, message, product) => {
  return `
<!doctype html>
<html>

<head>
	<meta name="viewport" content="width=device-width">
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<link href="https://fonts.googleapis.com/css?family=Gothic+A1:300,500,700" rel="stylesheet">
	<title>Italika | El motor de tu vida</title>
	<style>
		/* -------------------------------------
        INLINED WITH htmlemail.io/inline
    ------------------------------------- */
		/* -------------------------------------
        RESPONSIVE AND MOBILE FRIENDLY STYLES
    ------------------------------------- */

		.productos {
			margin: o auto;
			width: 100%;
			height: 275px;
			overflow: auto;
			display: inline-block;
			box-sizing: border-box;
			font-size: 16px;
			border-spacing: 0;
		}

		.productos thead tr th {
			width: 20%
		}

		.productos tbody tr {
			padding: 0px;
			border: none;
		}

		.productos tbody tr td {
			font-size: 1rem;
			width: 20px;
			height: 20px;
			padding: 10px;
			border: none;
			overflow: hidden;
		}


		table.striped tr {
			border-bottom: none;
		}

		table.striped>tbody>tr:nth-child(odd) {
			background-color: rgba(242, 242, 242, 0.5);
		}

		table.striped>tbody>tr>td {
			border-radius: 0;
		}




		@media only screen and (max-width: 620px) {

			.productos {
				margin: o auto;
				width: 100%;
				height: 250px;
				overflow: auto;
				display: inline-block;
				box-sizing: border-box;
				font-size: 12px;
			}

			.productos thead tr th {
				width: 25%
			}
			.productos tbody tr td {
				font-size: .6rem;
				width: 20px;
				height: 20px;
				padding: 10px;
				border: none;
				overflow: hidden;
			}


			table[class=body] h1 {
				font-size: 28px !important;
				margin-bottom: 10px !important;
			}
			table[class=body] p,
			table[class=body] ul,
			table[class=body] ol,
			table[class=body] td,
			table[class=body] span,
			table[class=body] a {
				font-size: 14px !important;
			}
			table[class=body] .wrapper,
			table[class=body] .article {
				padding: 10px !important;
			}
			table[class=body] .content {
				padding: 0 !important;
			}
			table[class=body] .container {
				padding: 0 !important;
				width: 100% !important;
			}
			table[class=body] .main {
				border-left-width: 0 !important;
				border-radius: 0 !important;
				border-right-width: 0 !important;
			}
			table[class=body] .btn table {
				width: 100% !important;
			}
			table[class=body] .btn a {
				width: 100% !important;
			}
			table[class=body] .img-responsive {
				height: auto !important;
				max-width: 100% !important;
				width: auto !important;
			}
		}

		@media screen and (max-width: 500px) {
			table[class=body] h1 {
				font-size: 28px !important;
				margin-bottom: 10px !important;
			}
			.stack-column-center {
				display: block !important;
				width: 100% !important;
				max-width: 100% !important;
				direction: ltr !important;
			}
		}

		/* -------------------------------------
        PRESERVE THESE STYLES IN THE HEAD
    ------------------------------------- */

		@media all {
			.ExternalClass {
				width: 100%;
			}
			.ExternalClass,
			.ExternalClass p,
			.ExternalClass span,
			.ExternalClass font,
			.ExternalClass td,
			.ExternalClass div {
				line-height: 100%;
			}
			.apple-link a {
				color: #aa82ff !important;
				font-family: inherit !important;
				font-size: inherit !important;
				font-weight: inherit !important;
				line-height: inherit !important;
				text-decoration: none !important;
			}
			.btn-primary table td:hover {
				background-color: #2e2654 !important;
			}
			.btn-primary a:hover {
				background-color: #2e2654 !important;
				border-color: #2e2654 !important;
			}

		}
	</style>
</head>

<body class="body" style="min-height: 100vh; font-family: 'Gothic A1', sans-serif; -webkit-font-smoothing: antialiased; font-size: 14px; line-height: 1.4; margin: 0; padding: 0; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%;">
	<table border="0" cellpadding="0" cellspacing="0" class="body" style="border-collapse: separate; mso-table-lspace: 0pt; mso-table-rspace: 0pt; width: 100%;">
		<tr>
			<td style="font-family: 'Gothic A1', sans-serif; font-size: 14px; vertical-align: top;">&nbsp;</td>
			<td class="container" style="font-family: 'Gothic A1', sans-serif; font-size: 14px; vertical-align: top; display: block; margin: 0 auto; max-width: 800px; width: 800px;">
				<div style="background-size: cover; background-repeat: no-repeat; background-image:url(https://images.unsplash.com/photo-1543424154-ca5491635a96?ixlib=rb-0.3.5&ixid=eyJhcHBfaWQiOjEyMDd9&s=05c31e7b4de4fab0fa931cb0a3c00a95&auto=format&fit=crop&w=1951&q=80);">
					<div class="content" style="box-sizing: border-box; display: block;">
						<table align="center" border="0" cellpadding="0" cellspacing="0" style="border-collapse: separate; mso-table-lspace: 0pt; mso-table-rspace: 0pt; width: 100%; margin-bottom: 25px">
							<tr>
								<td colspan="2" style="text-align: -webkit-center">
									<img alt="logo" resizemode="contain" style="height: 90px; text-align: baseline; padding-top: 40px" src="https://as-public-images.s3.amazonaws.com/logo_italika.png" />
							</td>
						</tr>
					</table>
				</div>
				<div class="content" style="box-sizing: border-box; display: block; padding-top:50px; padding-bottom:50px;">
					<!-- START CENTERED WHITE CONTAINER -->
					<table class="main" style=" margin: auto; border-collapse: separate; mso-table-lspace: 0pt; mso-table-rspace: 0pt; width: 75%; background: #ffffff; border-radius: 3px; box-shadow: 0 3px 5px rgba(0, 0, 0, 0.3);">
						<!-- START MAIN CONTENT AREA -->
						<tr>
							<td class="wrapper" style="font-family: 'Gothic A1', sans-serif; font-size: 14px; vertical-align: top; box-sizing: border-box; padding: 20px;">
								<table border="0" cellpadding="0" cellspacing="0" style="border-collapse: separate; mso-table-lspace: 0pt; mso-table-rspace: 0pt; width: 100%;">
									<tr>
										<td>
											
											<h2 style="font-family: 'Gothic A1', sans-serif;  text-align: center; margin: 0; color: #474747; margin-bottom: 10px;">${subject}</h2>
											
										</td>
									</tr>
									<tr>
										<td colspan="2" style="font-family: 'Gothic A1', sans-serif; font-size: 14px; vertical-align: top;">

											<table border="0" cellpadding="0" cellspacing="0" style="border-collapse: separate; mso-table-lspace: 0pt; mso-table-rspace: 0pt; width: 80%; margin:0 auto; box-sizing: border-box;">
												<tbody>
													<tr>
														<td align="center" style="text-align: center;">
															<div style="margin: 16px 0;">${message}</div>
														</td>
													</tr>
												</tbody>
											</table>

											<table border="0" cellpadding="0" cellspacing="0" class="btn btn-primary" style="border-collapse: separate; mso-table-lspace: 0pt; mso-table-rspace: 0pt; width: 50%; margin:0 25%; box-sizing: border-box;">
												<tbody>
													<tr>
														<td align="center" style="font-family: 'Gothic A1', sans-serif; font-size: 14px; vertical-align: top; padding-bottom: 15px;">
															<table border="0" cellpadding="0" cellspacing="0" style="border-collapse: separate; mso-table-lspace: 0pt; mso-table-rspace: 0pt; width: auto;">
																<tbody>
																	<tr>
																		<td style="font-family: 'Gothic A1', sans-serif; font-size: 14px; vertical-align: top; border-radius: 15px; text-align: center;">
																			<a href="${product.priceLink}" target="_blank" style="display: inline-block; color: #ffffff; background-color: #aa82ff; border-radius: 8px; box-sizing: border-box; cursor: pointer; text-decoration: none; font-size: 16px; font-weight: 500; margin: 0; padding: 12px 25px; line-height: 16px;">Link del producto</a>
																		</td>
																	</tr>
																</tbody>
															</table>
														</td>
													</tr>
												</tbody>
											</table>
										</td>
									</tr>
									<tr>
										<td colspan="2" valign="top" width="100%" style="padding: 10px; background-color: #ffffff;">
											<table role="presentation" cellspacing="0" cellpadding="0" border="0" width="100%">
												<tr>
													<td class="stack-column-center">
														<table role="presentation" cellspacing="0" cellpadding="0" border="0">
															<tr style="text-align: center">
																<td style="padding: 0 10px;">
																	<img alt="logo" resizemode="contain" style="width: 200px" src="https://as-public-images.s3.amazonaws.com/background_italika.jpg" />
																</td>
															</tr>
														</table>
													</td>
													<td class="stack-column-center">
														<table role="presentation" cellspacing="0" cellpadding="0" border="0">
															<tr style="text-align: center">
																<td style="padding: 0 10px;">
																	<p align="left" style="color: #333333; font-size: 20px; font-weight: bold; text-align: left; margin: 0">¿Necesitas Ayuda?</p>
																	<p align="left" style="color: #5E5E5E; margin-top: 2px;">Escríbenos a
																		<a href="mailto:soporte@centralfresh.mx" style="color: #aa82ff">soporte@centralfresh.mx</a> o comunícate al teléfono 55 8027 9806</p>
																</td>
															</tr>
														</table>
													</td>
												</tr>
											</table>
										</td>
									</tr>
								</table>
							</td>
						</tr>
					</table>
				</div>



				<!--Actualizar los link de redes sociales y liga de iconos-->


				<div class="footer" style="color: white; margin-top: 25px; text-align: center; background-color: #000000">
					<table border="0" cellpadding="0" cellspacing="0" style="border-collapse: separate; mso-table-lspace: 0pt; mso-table-rspace: 0pt; width: 100%;">
						<tr style="text-align: center">
							<td colspan="4" style="padding: 15px">
							</td>
						</tr>
						<tr>
							<td style="width: 20%" />
							<td class="content-block" style=" border-top: 2px solid #5E5E5E; font-family: 'Gothic A1', sans-serif; vertical-align: top; padding-bottom: 10px; padding-top: 10px; font-size: 12px; color: #999999; text-align: center;">
								<span class="apple-link" style="color: white; font-size: 12px; text-align: center;">Copyright© 2018 Plukke, Todos los derechos reservados.
							</td>
							<td style="width: 20%" />
						</tr>
						<tr>
							<td />
							<td class="content-block" style="font-family: 'Gothic A1', sans-serif; vertical-align: top; padding-bottom: 10px; padding-top: 10px; font-size: 12px; color: #999999; text-align: justify;">
								</td>
							<td />
						</tr>

						<tr>
							<td />
							<td class="content-block powered-by" style="color: white; font-family: 'Gothic A1', sans-serif; vertical-align: top; padding-bottom: 10px; padding-top: 10px; font-size: 12px; color: #999999; text-align: center;">
								<span style="color: white">Powered by </span>
								<a href="https://www.plukke.mx" target="_blank" style="color: white; font-size: 12px; text-align: center; text-decoration: none;">Plukke</a>.
							</td>
							<td />
						</tr>
					</table>
				</div>
			</div>
			</td>
		<td style="font-family: 'Gothic A1', sans-serif; font-size: 14px; vertical-align: top;">&nbsp;</td>
	 </tr>
	</table>
</body>

</html>
`;
};
