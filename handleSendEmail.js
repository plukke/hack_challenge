const AWS = require("aws-sdk");
AWS.config.update({ region: "us-east-1" });
const sendPromise = new AWS.SES({ apiVersion: "2010-12-01" });

module.exports = (address = ["plukkemx@gmail.com"], subject, data, source) => {
  const emailParams = {
    Destination: {
      /* required */
      ToAddresses: address
    },
    Message: {
      /* required */
      Body: {
        /* required */
        Html: {
          Charset: "UTF-8",
          Data: data
        }
      },
      Subject: {
        Charset: "UTF-8",
        Data: subject
      }
    },
    Source: source
    /* required */ // Cambiar por el dominio de AS Xpert
  };
  return new Promise((resolve, reject) => {
    sendPromise
      .sendEmail(emailParams)
      .promise()
      .then(data => {
        resolve(data);
        console.log(data);
      })
      .catch(err => {
        reject(err.stack);
        console.log(err, err.stack);
      });
  });
};
