module.exports = {
	LastIntentInterceptor: {
		process(handlerInput, response) {
			const { request } = handlerInput.requestEnvelope;
			if (request.type === 'IntentRequest' && request.intent.name !== 'SelectIntent') {
				console.log('Intent Response interceptor', request.intent.name);
				console.log('Response interceptor', response);
				const attributes = handlerInput.attributesManager.getSessionAttributes();
				switch (request.intent.name) {
					case 'DetailProductIntent':
						attributes['lastProducts'] = [];
						break;
					case 'ListProductsIntent':
						attributes['recommendations'] = [];
						break;
					default:
						break;
				}
				attributes['lastIntent'] = request.intent.name;
				// handlerInput.attributesManager.setSessionAttributes(attributes);
			}
		}
	}
};
