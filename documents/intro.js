const { resources, styles } = require('./rootResources');

module.exports = {
    "type": "APL",
    "version": "1.0",
    "theme": "dark",
    "import": [],
    resources,
    styles,
    "mainTemplate": {
        "parameters": [
            "payload"
        ],
        "items": [
            {
                "type": "Container",
                "height": "100vh",
                "width": "100vw",
                "direction": "column",
                "alignItems": "center",
                "justifyContent": "center",
                "items": [
                    {
                        "type": "Image",
                        "position": "absolute",
                        "height": "100vh",
                        "width": "100vw",
                        "overlayColor": "rgba(0, 32, 91, 0.4)",
                        "source": "${payload.bodyTemplateIntro.backgroundImage.sources[0].url}",
                        "scale": "best-fill"
                    },
                    {
                        "type": "Image",
                        "scale": "fit",
                        "id": "logoIntroImage",
                        "height": "100%",
                        "width": "90%",
                        "source": "${payload.bodyTemplateIntro.image.sources[0].url}",
                        "opacity": 0
                    }
                ]
            }    
        ]
    }
}