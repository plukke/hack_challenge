const { resources, styles } = require('./rootResources');


module.exports = {
    "type": "APL",
    "version": "1.0",
    "theme": "dark",
    "import": [
        {
            "name": "alexa-layouts",
            "version": "1.0.0"
        }
    ],
    resources, 
    styles,
    "layouts": {
        "FullHorizontalListItem": {
            "parameters": [
                "listLength"
            ],
            "item": [
                {
                    "type": "TouchWrapper",
                    "onPress": {
                      "type": "SendEvent",
                      "arguments": [
                        "ItemSelectedProduct",
                        "${data.textContent.index.text}"
                      ]
                    },
                    "item": {
                        "type": "Container",
                        "height": "100vh",
                        "width": "100vw",
                        "alignItems": "center",
                        "justifyContent": "start",
                        "items": [
                            {
                                "type": "Image",
                                "position": "absolute",
                                "height": "100vh",
                                "width": "100vw",
                                "overlayColor": "rgba(0,0,0, 0.6)",
                                "source": "${data.image.sources[0].url}",
                                "scale": "best-fill"
                            },
                            {
                                "type": "AlexaHeader",
                                "headerTitle": "${title}",
                                "headerAttributionImage": "${logo}"
                            },
                            {
                                "type": "Text",
                                "text": "${data.textContent.primaryText.text}",
                                "style": "textStyleBody",
                                "maxLines": 2,
                                "maxWidth": "90vw"
                            },
                            {
                                "type": "Text",
                                "text": "${data.textContent.secondaryText.text}",
                                "style": "textStyleDetails",
                                "maxWidth": "85vw",
                                "maxLines": 5,
                                "grow": 1
                            },
                            {
                                "type": "Text",
                                "text": "${data.textContent.footerText.text}",
                                "style": "textStyleFooter"
                            },
                            {
                                "type": "Text",
                                "text": "${data.textContent.index.text} | ${listLength}",
                                "paddingBottom": "20dp",
                                "color": "white",
                                "spacing": "5dp"
                            }
                        ]
                    }
                }
            ]
        },
        "HorizontalListItem": {
            "item": [
                {
                    "type": "Container",
                    "maxWidth": 528,
                    "minWidth": 312,
                    "paddingLeft": 16,
                    "paddingRight": 16,
                    "height": "100%",
                    "id": "scrollContainer",
                    "items": [
                        {
                            "type": "Image",
                            "source": "${data.image.sources[0].url}",
                            "height": "50vh",
                            "width": "50vh"
                        },
                        {
                            "type": "Text",
                            "text": "<b>${data.textContent.index.text}.</b> ${data.textContent.primaryText.text}",
                            "style": "textStyleSecondary",
                            "maxLines": 1,
                            "spacing": 12
                        },
                        {
                            "type": "Text",
                            "text": "${data.textContent.secondaryText.text}",
                            "style": "textStyleDetails",
                            "spacing": 4
                        }
                    ]
                }
            ]
        },
        "ListTemplate2": {
            "parameters": [
                "backgroundImage",
                "title",
                "logo",
                "hintText",
                "listData"
            ],
            "items": [
                {
                    "when": "${viewport.shape == 'round'}",
                    "type": "Container",
                    "height": "100%",
                    "width": "100%",
                    "items": [
                        {
                            "type": "Pager",
                            "scrollDirection": "horizontal",
                            "data": "${listData}",
                            "height": "100%",
                            "width": "100%",
                            "numbered": true,
                            "id": "pagerComponentId",
                            "item": [
                                {
                                    "type": "FullHorizontalListItem",
                                    "listLength": "${payload.listTemplate2ListData.listPage.listItems.length}"
                                }
                            ]
                        }
                    ]
                },
                {
                    "type": "Container",
                    "height": "100vh",
                    "width": "100vw",
                    "items": [
                        {
                            "type": "Image",
                            "source": "${backgroundImage}",
                            "scale": "best-fill",
                            "width": "100vw",
                            "height": "100vh",
                            "overlayColor": "rgba(29,57,113, 0.6)",
                            "position": "absolute"
                        },
                        {
                            "type": "AlexaHeader",
                            "headerTitle": "${title}",
                            "headerAttributionImage": "${logo}"
                        },
                        {
                            "type": "Pager",
                            "scrollDirection": "horizontal",
                            "paddingLeft": "@marginLeft",
                            "paddingRight": "@marginRight",
                            "data": "${listData}",
                            "height": "70vh",
                            "width": "100%",
                            "numbered": true,
                            "id": "pagerComponentId",
                            "item": [
                                {
                                    "type": "HorizontalListItem"
                                }
                            ]
                        },
                        {
                            "type": "AlexaFooter",
                            "footerHint": "${payload.listTemplate2ListData.hintText}"
                        }
                    ]
                }
            ]
        }
    },
    "mainTemplate": {
        "parameters": [
            "payload"
        ],
        "item": {
            "type": "ListTemplate2",
            "backgroundImage": "${payload.listTemplate2Metadata.backgroundImage.sources[0].url}",
            "title": "${payload.listTemplate2Metadata.title}",
            "hintText": "${payload.listTemplate2Metadata.hintText}",
            "logo": "${payload.listTemplate2Metadata.logoUrl}",
            "listData": "${payload.listTemplate2ListData.listPage.listItems}"
        }
    }
}