// This sample demonstrates handling intents from an Alexa skill using the Alexa Skills Kit SDK (v2).
// Please visit https://alexa.design/cookbook for additional examples on implementing slots, dialog management,
// session persistence, api calls, and more.
const Alexa = require('ask-sdk-core');
const util = require('./util');
const handleSendEmail = require('./handleSendEmail');
const emailTemplate = require('./emailTemplate');
const { LastIntentInterceptor } = require('./interceptors.js');

const datasourceIntro = require('./datasources/intro');
const handleProductsDatasource = require('./datasources/listProducts');
// const handleDetailProductDatasource = require('./datasources/detailProduct');

const LaunchRequestHandler = {
	canHandle(handlerInput) {
		return Alexa.getRequestType(handlerInput.requestEnvelope) === 'LaunchRequest';
	},
	async handle(handlerInput) {
		const supportsAPL = util.supportsDisplay(handlerInput);
		if (supportsAPL) {
			return handlerInput.responseBuilder
				.speak(INTRO_MESSAGE)
				.addDirective({
					type: 'Alexa.Presentation.APL.RenderDocument',
					token: 'introToken',
					version: '1.0',
					document: require('./documents/intro.js'),
					datasources: datasourceIntro
				})
				.addDirective({
					type: 'Alexa.Presentation.APL.ExecuteCommands',
					token: 'introToken',
					commands: [
						{
							type: 'AnimateItem',
							easing: 'ease-in-out',
							duration: 250,
							componentId: 'logoIntroImage',
							value: [
								{
									property: 'opacity',
									to: 1
								},
								{
									property: 'transform',
									from: [
										{
											scale: 0.7
										}
									],
									to: [
										{
											scale: 1
										}
									]
								}
							]
						}
					]
				})
				.reprompt(HELP_REPROMPT)
				.getResponse();
		}
		return handlerInput.responseBuilder.speak(INTRO_MESSAGE).reprompt(HELP_REPROMPT).getResponse();
	}
};

const SearchProductIntentHandler = {
	canHandle(handlerInput) {
		return (
			Alexa.getRequestType(handlerInput.requestEnvelope) === 'IntentRequest' &&
			Alexa.getIntentName(handlerInput.requestEnvelope) === 'SearchProductIntent'
		);
	},

	async handle(handlerInput) {
		let speechText = '';
		const { attributesManager, requestEnvelope } = handlerInput;
		const sessionAttributes = attributesManager.getSessionAttributes();
		const producto = Alexa.getSlotValue(requestEnvelope, 'producto');
		const contactData = Alexa.getSlotValue(requestEnvelope, 'contactData');
		console.log(producto, contactData);

		// email provided
		const email = contactData.replace('arroba', '@').split(' ').join('');
		console.log('parsedEmail', email);
		//validate email
		if (!util.emailRegex.test(email)) {
			speechText = 'No pude entender tu correo, por favor intenta nuevamente';
			return handlerInput.responseBuilder.speak(speechText).reprompt('¿Cuál es tu correo?').getResponse();
		}

		try {
			const speech = 'Por favor espera mientras recupero la moto que elegiste.';
			await util.callDirectiveService(handlerInput, speech);
		} catch (err) {
			console.log(err);
		}

		const query = {
			bool: {
				must: {
					multi_match: {
						query: producto,
						type: 'best_fields',
						operator: 'AND',
						fields: [ 'name^3', 'name.autocompleted', 'slogan', 'slogan.autocompleted' ]
					}
				}
			}
		};

		const response = await util.requestES(query);
		console.log('response', response);

		const { hits: { hits } = {} } = response ? JSON.parse(response) : {};

		let result = [];
		hits.map((hit) => {
			result = [ ...result, hit._source ];
		});

		if (result && result.length) {
			console.log('RESULT', JSON.stringify(result[0], null, 2));
			const selectedProduct = result[0];
			sessionAttributes['selectedProduct'] = selectedProduct;

			const subject = `Italika | ${selectedProduct.name}`;
			const emailMessage = `Puedes ver todos los detalles de ${selectedProduct.name} en ${selectedProduct.priceLink}`;
			const data = emailTemplate(subject, emailMessage, selectedProduct);
			const source = 'no-reply-consumers@xuup.mx';
			try {
				await handleSendEmail([ email ], subject, data, source);
			} catch (err) {
				console.log('Error al enviar el correo', err);
			}

			speechText = `La información de la motocicleta ${selectedProduct.name} fue enviada a tu correo.`;
			return handlerInput.responseBuilder.speak(speechText).getResponse();
		} else {
			speechText = 'No pude encontrar el producto que mencionas, por favor intenta con otro';
			return handlerInput.responseBuilder.speak(speechText).getResponse();
		}
	}
};

const SendInfoIntentHandler = {
	canHandle(handlerInput) {
		return (
			Alexa.getRequestType(handlerInput.requestEnvelope) === 'IntentRequest' &&
			Alexa.getIntentName(handlerInput.requestEnvelope) === 'SendInfoIntent'
		);
	},

	async handle(handlerInput) {
		let speechText = '';
		const { attributesManager, requestEnvelope } = handlerInput;
		const sessionAttributes = attributesManager.getSessionAttributes();
		const contactData = Alexa.getSlotValue(requestEnvelope, 'contactData');
		console.log('CONTACT DATA', contactData);

		// if (!contactData) {
		//   speechText = "No tengo tu correo para enviarte la información";
		//   return handlerInput.responseBuilder
		//     .speak(speechText)
		//     .reprompt("¿Dime tu correo por favor?")
		//     .getResponse();
		// }

		// email provided
		const email = contactData.replace(' ', '').replace('arroba', '@');
		//validate email
		if (!util.emailRegex.test(email)) {
			speechText = 'No pude entender tu correo, por favor intenta nuevamente';
			return handlerInput.responseBuilder.speak(speechText).reprompt('¿Cuál es tu correo?').getResponse();
		}

		const product = sessionAttributes['selectedProduct'];
		console.log('PRODUCT', product);

		if (!product) {
			speechText = 'No elegiste producto para enviarte la información, por favor intenta nuevamente';
			return handlerInput.responseBuilder.speak(speechText).getResponse();
		} else {
			speechText = `La información de la motocicleta ${product.name} fue enviada a tu correo.`;
			return handlerInput.responseBuilder.speak(speechText).getResponse();
		}
	}
};

const RecommendationItentHandler = {
	canHandle(handlerInput) {
		return (
			Alexa.getRequestType(handlerInput.requestEnvelope) === 'IntentRequest' &&
			Alexa.getIntentName(handlerInput.requestEnvelope) === 'RecommendationIntent'
		);
	},

	async handle(handlerInput) {
		let speechText = '';
		const { attributesManager, requestEnvelope } = handlerInput;
		const sessionAttributes = attributesManager.getSessionAttributes();

		try {
			const speech = 'Por favor espera mientras obtengo tus recomendaciones.';
			await util.callDirectiveService(handlerInput, speech);
		} catch (err) {
			console.log(err);
		}

		console.log('attributeManager', attributesManager);
		console.log('requestEnvelope', JSON.stringify(requestEnvelope, null, 2));
		const modeloSlot = Alexa.getSlot(requestEnvelope, 'modelo');
		// categoria seleccionada
		const modeloId = modeloSlot.resolutions.resolutionsPerAuthority[0].values[0].value.id;
		// presupuesto maximo del usuario
		const presupuesto = Alexa.getSlotValue(requestEnvelope, 'presupuesto');
		// caracteristica mas importante
		const caracteristicaPrincipal = Alexa.getSlotValue(requestEnvelope, 'caracteristicaPrincipal');

		console.log(presupuesto, caracteristicaPrincipal, modeloId.replace('_', ' '));

		const query = {
			bool: {
				filter: [
					{ term: { 'model.keyword': modeloId.replace('_', ' ') } },
					{ range: { price: { lte: presupuesto } } }
				]
			}
		};

		// Products query
		const sort = caracteristicaPrincipal === 'velocidad' ? [ { speed: 'DESC' } ] : [ { performance: 'DESC' } ];
		const response = await util.requestES(query, sort);
		console.log('response', response);

		const { hits: { hits } = {} } = response ? JSON.parse(response) : {};

		// let result = [];
		// hits.map((hit) => {
		// 	result = [ ...result, hit._source ];
		// });

		if (hits && hits.length) {
			console.log('RESULT', JSON.stringify(hits.slice(0, 3), null, 2));
			sessionAttributes['recommendations'] = hits.slice(0, 3);
			return ListProductsHandler.handle(handlerInput);
		} else {
			speechText = 'No pude obtener recomendaciones, intenta nuevamente';
			return handlerInput.responseBuilder.speak(speechText).withShouldEndSession(true).getResponse();
		}
	}
};

const ListProductsHandler = {
	canHandle(handlerInput) {
		return (
			Alexa.getRequestType(handlerInput.requestEnvelope) === 'IntentRequest' &&
			Alexa.getIntentName(handlerInput.requestEnvelope) === 'ListProductsIntent'
		);
	},
	async handle(handlerInput) {
		const { attributesManager, requestEnvelope } = handlerInput;
		const sessionAttributes = attributesManager.getSessionAttributes();

		let speakOutput = '';

		try {
			const speech = 'Por favor espera mientras se cargan los productos.';
			await util.callDirectiveService(handlerInput, speech);
		} catch (err) {
			console.log(err);
		}
		// const attributes = handlerInput.attributesManager.getSessionAttributes();
		const modeloSlot = Alexa.getSlot(requestEnvelope, 'modelo');
		// categoria seleccionada
		const modeloId = modeloSlot.resolutions.resolutionsPerAuthority[0].values[0].value.id;

		console.log('SESSION ATTRIBUTES:', sessionAttributes);
		let actualHits = [];
		let actualTotal = actualHits.length;

		if (sessionAttributes['recommendations'] && sessionAttributes['recommendations'].length) {
			actualHits = sessionAttributes['recommendations'];
			actualTotal = actualHits.length;
		} else {
			const query = {
				bool: {
					must: {
						match_all: {}
					},
					filter: { term: { 'model.keyword': modeloId.replace('_', ' ') } }
				}
			};
			const data = await util.requestES(query);
			const { hits: { total, hits = [] } = {} } = JSON.parse(data);
			actualHits = hits;
			actualTotal = total;
		}

		// const filterHits = actualHits.filter(({ _source }) => _source.price > 0);

		sessionAttributes['lastProducts'] = actualHits;
		console.log('FILTER HITS', actualHits);

		if (actualHits && actualHits.length) {
			actualHits.forEach(({ _source }, index) => {
				speakOutput += `${index + 1}. ${_source.name}, con un precio de lista de ${_source.price} pesos. `;
			});
			speakOutput = `Aquí tienes los productos: ${speakOutput.replace(
				/&+/g,
				'y'
			)}. Si deseas consultar más detalle del producto intenta con: Alexa, detalle del número dos, o simplemente da click en el producto.`;
		} else {
			speakOutput = 'Por el momento no hay ningun producto';
		}

		const datasources = handleProductsDatasource(actualHits, actualTotal);

		const supportsAPL = util.supportsDisplay(handlerInput);

		console.log('DATA SOURCES', JSON.stringify(datasources));

		if (supportsAPL) {
			return (
				handlerInput.responseBuilder
					.speak(speakOutput)
					.addDirective({
						type: 'Alexa.Presentation.APL.RenderDocument',
						token: 'productsToken',
						version: '1.0',
						document: require('./documents/listProducts.js'),
						datasources
					})
					// .addDirective({
					// 	type: 'Alexa.Presentation.APL.ExecuteCommands',
					// 	token: 'productsToken',
					// 	commands: [
					// 		{
					// 			type: 'AutoPage',
					// 			componentId: 'pagerComponentId',
					// 			duration: 2500
					// 		}
					// 	]
					// })
					.reprompt('¿Qué número de moto deseas selecionar?')
					.getResponse()
			);
		}

		return handlerInput.responseBuilder.speak(speakOutput).reprompt('¿Qué número deseas selecionar?').getResponse();
	}
};
const DetailProductHandler = {
	canHandle(handlerInput) {
		return (
			(Alexa.getRequestType(handlerInput.requestEnvelope) === 'IntentRequest' &&
				Alexa.getIntentName(handlerInput.requestEnvelope) === 'DetailProductIntent') ||
			Alexa.getRequestType(handlerInput.requestEnvelope) === 'Alexa.Presentation.APL.UserEvent'
		);
	},
	async handle(handlerInput) {
		const { attributesManager, requestEnvelope } = handlerInput;
		const sessionAttributes = attributesManager.getSessionAttributes();
		const producto = Alexa.getSlotValue(requestEnvelope, 'producto');

		let speakOutput = 'Aqui tienes el detalles del producto';
		let dataProduct = sessionAttributes['product'];

		console.log('SESSION ATTRIBUTES', sessionAttributes);
		console.log('SLOT VALUE', producto);
		if (!dataProduct) {
			/** Buscar en el elastic search */
			const query = {
				bool: {
					must: {
						multi_match: {
							query: producto,
							type: 'best_fields',
							operator: 'AND',
							fields: [ 'name^3', 'name.autocompleted', 'slogan', 'slogan.autocompleted' ]
						}
					}
				}
			};

			const response = await util.requestES(query, sort);
			console.log('response', response);

			const { hits: { hits } = {} } = response ? JSON.parse(response) : {};
			dataProduct = (hits[0] && hits[0]._source) || {};
		}
		console.log('DATA PRODUCT', dataProduct);

		const supportsAPL = util.supportsDisplay(handlerInput);

		// const datasources = handleDetailProductDatasource(dataProduct);

		if (supportsAPL) {
			return (
				handlerInput.responseBuilder
					.speak(speakOutput)
					// .addDirective({
					// 	type: 'Alexa.Presentation.APL.RenderDocument',
					// 	token: 'detailProductToken',
					// 	version: '1.0',
					// 	document: require('./documents/detailProduct.js'),
					// 	datasources
					// })
					// .addDirective({
					// 	type: 'Alexa.Presentation.APL.ExecuteCommands',
					// 	token: 'detailProductToken',
					// 	commands: [
					// 		{
					// 			type: 'SpeakItem',
					// 			componentId: 'productBody',
					// 			highlightMode: 'line'
					// 		}
					// 	]
					// })
					.reprompt('¿Te puedo ayudar en algo más?')
					.withShouldEndSession(true)
					.getResponse()
			);
		}

		return handlerInput.responseBuilder.speak(speakOutput).reprompt('¿Qué número deseas selecionar?').getResponse();
	}
};

const SelectIntentHandler = {
	canHandle(handlerInput) {
		return (
			handlerInput.requestEnvelope.request.type === 'IntentRequest' &&
			handlerInput.requestEnvelope.request.intent.name === 'SelectIntent'
		);
	},
	handle(handlerInput) {
		const { attributesManager, requestEnvelope } = handlerInput;
		const sessionAttributes = attributesManager.getSessionAttributes();
		const selectedItemValue = Alexa.getSlotValue(requestEnvelope, 'selectedItem');
		const totalProducts = sessionAttributes['lastProducts'].length;

		console.log('SLOT VALUE SELECT', selectedItemValue);

		if (selectedItemValue > totalProducts) {
			return handlerInput.responseBuilder
				.speak(`Por favor elije un número entre 1 y ${totalProducts}`)
				.reprompt()
				.getResponse();
		}

		const product = sessionAttributes['lastProducts'][selectedItemValue - 1];
		console.log('SELECTED PRODUCT', product);

		sessionAttributes['product'] = product;
		return DetailProductHandler.handle(handlerInput);

		// return handlerInput.responseBuilder.speak(FALLBACK_MESSAGE).reprompt(HELP_REPROMPT).getResponse();
	}
};

const UserEventHandler = {
	canHandle(handlerInput) {
		return handlerInput.requestEnvelope.request.type === 'Alexa.Presentation.APL.UserEvent';
	},
	handle(handlerInput) {
		const { attributesManager, requestEnvelope } = handlerInput;
		const [ event, data ] = requestEnvelope.request.arguments;
		const sessionAttributes = attributesManager.getSessionAttributes();
		// Any cleanup logic goes here.
		console.log('USER EVENT TOUCH', event);
		console.log('SESSION ATTRIBUTES', sessionAttributes);
		switch (event) {
			case 'ItemSelectedProduct':
				/** Meter en los session attributes */
				const product = sessionAttributes['lastProducts'][data];
				sessionAttributes['product'] = product;
				return DetailProductHandler.handle(handlerInput);
			default:
				return handlerInput.responseBuilder.getResponse();
		}
	}
};

const HelpIntentHandler = {
	canHandle(handlerInput) {
		return (
			Alexa.getRequestType(handlerInput.requestEnvelope) === 'IntentRequest' &&
			Alexa.getIntentName(handlerInput.requestEnvelope) === 'AMAZON.HelpIntent'
		);
	},
	handle(handlerInput) {
		return handlerInput.responseBuilder.speak(HELP_MESSAGE).reprompt(HELP_REPROMPT).getResponse();
	}
};

const CancelAndStopIntentHandler = {
	canHandle(handlerInput) {
		return (
			Alexa.getRequestType(handlerInput.requestEnvelope) === 'IntentRequest' &&
			(Alexa.getIntentName(handlerInput.requestEnvelope) === 'AMAZON.CancelIntent' ||
				Alexa.getIntentName(handlerInput.requestEnvelope) === 'AMAZON.StopIntent')
		);
	},
	handle(handlerInput) {
		const speakOutput = STOP_MESSAGE;
		return handlerInput.responseBuilder.speak(speakOutput).withShouldEndSession(true).getResponse();
	}
};

const SessionEndedRequestHandler = {
	canHandle(handlerInput) {
		return Alexa.getRequestType(handlerInput.requestEnvelope) === 'SessionEndedRequest';
	},
	handle(handlerInput) {
		// Any cleanup logic goes here.
		return handlerInput.responseBuilder.getResponse();
	}
};

// The intent reflector is used for interaction model testing and debugging.
// It will simply repeat the intent the user said. You can create custom handlers
// for your intents by defining them above, then also adding them to the request
// handler chain below.
const IntentReflectorHandler = {
	canHandle(handlerInput) {
		return Alexa.getRequestType(handlerInput.requestEnvelope) === 'IntentRequest';
	},
	handle(handlerInput) {
		const intentName = Alexa.getIntentName(handlerInput.requestEnvelope);
		const speakOutput = `You just triggered ${intentName}`;

		return (
			handlerInput.responseBuilder
				.speak(speakOutput)
				//.reprompt('add a reprompt if you want to keep the session open for the user to respond')
				.getResponse()
		);
	}
};

// Generic error handling to capture any syntax or routing errors. If you receive an error
// stating the request handler chain is not found, you have not implemented a handler for
// the intent being invoked or included it in the skill builder below.
const ErrorHandler = {
	canHandle() {
		return true;
	},
	handle(handlerInput, error) {
		console.log(`~~~~ Error handled: ${error.stack}`);
		const speakOutput = `Perdón, Tengo problemas ejecutando lo que me pediste. Por favor intenta nuevamente.`;

		return handlerInput.responseBuilder.speak(speakOutput).reprompt(speakOutput).getResponse();
	}
};

const INTRO_MESSAGE =
	'Bienvenido a Itálika, El motor de tu vida. Pregúntame por la lista de productos, por una recomendación, por un producto en específico, te puedo enviar información de un producto en específico a tu correo o que te comunique con un asesor especialista. ¿En qué te puedo ayudar?';
const HELP_REPROMPT = '¿Cómo te puedo ayudar?';
const HELP_MESSAGE =
	'Puedes preguntarme por la lista de motos, o si quieres una recomendación solo dí...Alexa, recomiéndame o hazme una recomendación, también puedo comunicarte con un asesor especializado, ¿En qué te puedo ayudar?';
const STOP_MESSAGE =
	'Gracias por utilizar Itálica El motor de tu vida, <say-as interpret-as="interjection">vuelve pronto</say-as>';
const FALLBACK_MESSAGE =
	'Lo siento no te entendí, intenta de nuevo o dí "Alexa, ayuda" para conocer nuestras funciones.';

// The SkillBuilder acts as the entry point for your skill, routing all request and response
// payloads to the handlers above. Make sure any new handlers or interceptors you've
// defined are included below. The order matters - they're processed top to bottom.
exports.handler = Alexa.SkillBuilders
	.custom()
	.addRequestHandlers(
		LaunchRequestHandler,
		ListProductsHandler,
		DetailProductHandler,
		RecommendationItentHandler,
		SearchProductIntentHandler,
		SendInfoIntentHandler,
		SelectIntentHandler,
		UserEventHandler,
		HelpIntentHandler,
		CancelAndStopIntentHandler,
		SessionEndedRequestHandler,
		IntentReflectorHandler // make sure IntentReflectorHandler is last so it doesn't override your custom intent handlers
	)
	.addErrorHandlers(ErrorHandler)
	.withApiClient(new Alexa.DefaultApiClient())
	.addResponseInterceptors(LastIntentInterceptor)
	.lambda();
